from flask import Blueprint
from . import dhcp_leases_model
from net_suite import app, get_git_version, ModMetaData

# we are assuming, that at the time of loading the app is initializes and the config is loaded
bb_name = 'dhcp_leases'
dhcp_leases = Blueprint(bb_name, __name__, template_folder='templates')

# establish database connection and store it in the app
METADATA = ModMetaData(name=bb_name, mod_path=__name__, gitlab_url='https://git.scc.kit.edu/scc-net/net-suite/net-suite-dhcp_leases',
                       printable_name='DHCP-Leases', version=get_git_version(__file__),
                       contact_email='dns-betrieb@scc.kit.edu')
MENU = {METADATA.printable_name: 'dhcp_leases.overview'}
from . import views
