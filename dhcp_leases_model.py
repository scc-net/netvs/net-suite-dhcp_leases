import psycopg2
from psycopg2.extras import RealDictCursor
import configparser
import datetime


def get_timestamps_and_unseen_macs_from_data(range_data):
    """Strips the list of data entries down to a list of timestamps also stripping it from the 'None' entries
    :returns
        (list of timestamps, number of None occurrences in list)
    """
    last_seen_list = [elem['ts'] for elem in range_data]
    never_seen_counter = 0
    for i, date in enumerate(last_seen_list):
        if date is None:
            never_seen_counter += 1
    for elem in range(never_seen_counter):
        last_seen_list.remove(None)
    return last_seen_list, never_seen_counter


def count_ocurrence_of_macs_used_during_periods(range_data, periods=[1, 7, 30, 365], ):
    """Counts the occurrence of timestamps, that fall into the period from now to the time n days ago.
    Takes as input:
    periods: list of the number of days in the past. These days mark the intervall edges into which the timestamps are
    sorted
    returns a list of the occurrence in the different intervals"""
    periods.sort()
    occurrences = []
    for i, period in enumerate(periods):
        counter = 0
        if i == 0:
            for time in range_data:
                if time > datetime.datetime.now() - datetime.timedelta(days=periods[i]):
                    counter += 1
        else:
            for time in range_data:
                if datetime.datetime.now() - datetime.timedelta(
                        days=periods[i - 1]) > time > datetime.datetime.now() - datetime.timedelta(days=periods[i]):
                    counter += 1
        occurrences.append(counter)
    return occurrences


class DhcpLeaseDB(object):
    def __init__(self, database=None, user=None, host=None, port=None, password=None, ):
        """ An object that is a wrapper for the database that stores the DHCP leases and the mac addresses
        that are registered for that Range."""
        self.connection = psycopg2.connect(database=database, user=user, host=host,
                                           port=port, password=password, cursor_factory=RealDictCursor,
                                           sslmode='require')
        self.cursor = self.connection.cursor()

    def get_ranges(self):
        """gets all the ranges that are in the frickeldb"""
        self.cursor.execute("SELECT DISTINCT range FROM dhcp_lease")
        ranges = self.cursor.fetchall()
        return [elem['range'] for elem in ranges]

    def get_leases_in_range(self, dbrange, offset, limit):
        """ Returns the ranges that the User Manages as a """
        self.cursor.execute("SELECT mac, ip, ts FROM dhcp_lease WHERE range = %s OFFSET %s LIMIT %s",
                            (dbrange, offset, limit))
        return self.cursor.fetchall()

    def get_leasecount_in_range(self, dbrange):
        self.cursor.execute("SELECT COUNT(id) FROM dhcp_lease WHERE range = %s", (dbrange,))
        return self.cursor.fetchall()[0]['count']

    def get_mac_in_range(self, mac, dbrange):
        self.cursor.execute("SELECT * FROM dhcp_lease WHERE mac = %s AND range = %s", (mac, dbrange))
        data = self.cursor.fetchall()
        if len(data) == 0:
            return None
        else:
            return data

    def check_if_mac_registering(self, dbrange: str) -> bool:
        """This function checks if the given dbrange is in the mac_reg_range table and is therefor has the
        mac registraion feature"""
        self.cursor.execute("SELECT * FROM mac_reg_range WHERE name = %s", (dbrange,))
        data = self.cursor.fetchall()
        if len(data) == 0:
            return False
        else:
            return True

    def get_unused_macs(self, dbrange: str, offset: int, limit: int) -> list:
        """If a range has mac registering this function can pe used to get the ones that where registered
        but never seen."""
        self.cursor.execute("SELECT mac FROM dhcp_lease WHERE ts IS NULL AND range = %s OFFSET %s LIMIT %s",
                            (dbrange, offset, limit))
        data = self.cursor.fetchall()
        return data

    def get_used_macs(self, dbrange: str, offset: int, limit: int):
        """If a range has mac registering, this function returnes all the macs that have been seen before"""
        self.cursor.execute("SELECT mac, name, ts FROM dhcp_lease WHERE ts IS NOT NULL AND range = %s OFFSET %s"
                            " LIMIT %s", (dbrange, offset, limit))
        data = self.cursor.fetchall()
        return data

    def get_active_macs(self, dbrange: str) -> list:
        """Function returns all clients that have an IP address currently assigned to it."""
        self.cursor.execute("SELECT mac, name, ip FROM dhcp_lease WHERE ip IS NOT NULL AND range = %s ORDER BY ip",
                            (dbrange,))
        data = self.cursor.fetchall()
        if len(data) is 0:
            return None
        else:
            return data

    def get_active_mac_count(self, dbrange: str) -> list:
        """Function returns the number of clients that have an IP address currently assigned to it."""
        self.cursor.execute("SELECT COUNT(*) FROM dhcp_lease WHERE ip IS NOT NULL AND range = %s",
                            (dbrange,))
        data = self.cursor.fetchone()
        if len(data) is 0:
            return None
        else:
            return data['count']

    def get_active_mac_counts(self) -> list:
        """Function returns the number of clients that have an IP address currently assigned to it."""
        self.cursor.execute("SELECT COUNT(*), range FROM dhcp_lease WHERE ip IS NOT NULL GROUP BY range",
                            ())
        data = self.cursor.fetchall()
        return data

    def get_mac_to_ip(self, ip: str) -> list:
        """Function returns the mac and range for a given IP address"""
        self.cursor.execute("SELECT mac, range, name, ip FROM dhcp_lease WHERE ip = %s", (ip,))
        data = self.cursor.fetchall()
        return data


if __name__ == "__main__":
    """Tests and stuff to try out with enough data and a live connection to the db"""


    def parsedb_config(file, section='DEFAULT'):
        """ Parses a configfile and returns a dict containing its parameters.
        This is intended as an initializing finktion to the DhcpLeaseDB class."""
        config = configparser.ConfigParser()
        config.read(file)
        configlist = []
        for key in config[section]:
            configlist.append((key, config[section][key]))
        return dict(configlist)


    print('testing the database: ...')
    config = configparser.ConfigParser()
    config.read('dbconfig.ini')
    data_source = DhcpLeaseDB(**config['minidhcpvs'])
    print(data_source.get_leasecount_in_range("iss/1"))
    print(data_source.get_leases_in_range("iss/1", 20, 30))
    print(data_source.get_mac_in_range('44:78:3e:4d:a1:38', 'wkit-cl/3'))
    print(data_source.check_if_mac_registering("iss/1"))
    print(data_source.check_if_mac_registering("wkit-cl/3"))
    print(data_source.get_unused_macs('iss/1', 0, 50))
    print(data_source.get_used_macs('iss/1', 0, 50))
    print(data_source.get_active_macs('aruba-wkitx/1'))
    print(data_source.get_mac_to_ip('172.23.198.239'))

    # testing the plotting functions
    data_length = data_source.get_leasecount_in_range('iss/1')
    data = data_source.get_leases_in_range('iss/1', 0, data_length)
    filtered_data = get_timestamps_and_unseen_macs_from_data(data)
    occurrences = count_ocurrence_of_macs_used_during_periods(filtered_data[0])
    occurrences.append(filtered_data[1])
    print(occurrences)
    create_histogram_of_mac_usage(filtered_data[0])
    create_piechart_of_macusage(occurrences)
    plt.show()
