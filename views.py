from flask import render_template, g, current_app, request, url_for, abort
from net_suite.views import login_required, get_db_conn
from . import dhcp_leases
from . import dhcp_leases_model
from net_suite import db
from flask_breadcrumbs import register_breadcrumb
from net_suite.model import DBNetArea

def get_dhcpleaseDB():
    if not hasattr(g, 'dhcpleaseDB'):
        g.dhcpleaseDB = dhcp_leases_model.DhcpLeaseDB(database=current_app.config['MINIDHCPVS_DB'],
                                                     user=current_app.config['MINIDHCPVS_USER'],
                                                     host=current_app.config['MINIDHCPVS_HOST'],
                                                     port=current_app.config['MINIDHCPVS_PORT'],
                                                     password=current_app.config['MINIDHCPVS_PW'])
    return g.dhcpleaseDB


@dhcp_leases.teardown_request
def close_db(error):
    if hasattr(g, 'dhcpleaseDB'):
        g.dhcpleaseDB.connection.close()


@dhcp_leases.route("/")
@register_breadcrumb(dhcp_leases, '.', 'DHCP Leases')
@login_required
def overview():
    # get the user context from the app
    user = request.environ['beaker.session']['login']
    ranges = user.get_areas(db=db, connection=get_db_conn())
    dhcpDB = get_dhcpleaseDB()
    res_ranges = list()
    counts = dhcpDB.get_active_mac_counts()
    for dhcp_range in ranges:
        if dhcp_range.net.net.version != 4:
            continue
        name = dhcp_range.print_name
        count = 0
        for c in counts:
            if c['range'] == name:
                count = c['count']
        res_ranges.append({'range': dhcp_range,
                           'leases': count})

    # return the finished template
    return render_template('dhcp_leases/overview.html', ranges=res_ranges,
                           title='Aktive DHCP leases')


def view_net(*args, **kwargs):
    return [{'text': request.view_args['range'] + '/' + str(request.view_args['suffix']),
             'url': url_for('dhcp_leases.active_leases', range=request.view_args['range'],
                            suffix=request.view_args['suffix'])}]


@dhcp_leases.route("/<range>/<suffix>/active")
@register_breadcrumb(dhcp_leases, '.net', '', dynamic_list_constructor=view_net)
@login_required
def active_leases(range, suffix):
    dhcpDB = get_dhcpleaseDB()
    user = request.environ['beaker.session']['login']
    area = None
    if not user.has_permission('dns.admin'):
        areas =user.get_areas(db, get_db_conn())
        for a in areas:
            if a.net.net.version == 4 and a.name == range and a.suffix == int(suffix):
                area = a
                break
    else:
        area = DBNetArea.get_by_name(db=db, connection=get_db_conn(), name=range, suffix=suffix)
        if not area.net.net.version == 4:
            area = None
    # 1. Create the list of active dhcp leases in the different ranges
    if area is None:
        return abort(404)
    leases = dhcpDB.get_active_macs(range + '/' + str(suffix))
    return render_template('dhcp_leases/active_leases.html', title='Aktive DHCP leases: {}/{}'.format(range, suffix),
                           leases=leases, area=area,
                           subtitle='<code>{}</code>, {} aktive Leases'.format(area.net.net, len(leases) if leases else '0'))
